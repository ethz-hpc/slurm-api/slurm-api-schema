from typing import Annotated

from pydantic import BaseModel, StringConstraints

from .user import USER_REGEX


class Submit(BaseModel):
    user: Annotated[str, StringConstraints(pattern=USER_REGEX)]
    # WARNING script is not sanitized => never use it to
    # run a command! Always write into a file
    script: str
