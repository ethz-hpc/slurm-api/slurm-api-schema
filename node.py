import logging
from datetime import datetime
from enum import IntFlag, auto
from typing import Annotated

from pydantic import BaseModel, StringConstraints

from .reason import REASON_REGEX

# Allow only a to z, 0 to 9 and dashes
NODE_REGEX = r"^[a-zA-Z0-9-]+$"

# Needs a lot of special characters (, ), _, :, =, -
NODE_GROUP_REGEX = r"^[a-zA-Z0-9-()_,:=]+$"


class NodeState(IntFlag):
    NONE = 0
    # Node open
    IDLE = auto()
    MIXED = MIX = auto()
    RESV = RESERVED = auto()
    MAINT = MAINTENANCE = auto()
    ALLOC = ALLOCATED = auto()
    COMP = COMPLETING = auto()
    PLANNED = auto()
    CLOUD = auto()
    # Node closed
    DRAINED = auto()
    DRAIN = DRAINING = auto()
    DOWN = auto()
    FAIL = auto()
    ERROR = auto()
    FUTURE = FUTR = auto()
    NPC = auto()
    PERFCTRS = auto()
    NO_RESPOND = NOT_RESPONDING = auto()
    POWER_DOWN = POWERING_DOWN = POWERED_DOWN = auto()
    POWERING_UP = auto()
    REBOOT_ISSUED = auto()
    REBOOT_REQUESTED = auto()
    UNK = UNKNOWN = auto()

    def is_open(self) -> bool:
        if self.value == NodeState.NONE:
            return False
        return self.value < self.DRAINED.value

    @classmethod
    def get_state_from_text(cls, text: str):
        state = NodeState.NONE
        for current in text.split("+"):
            try:
                state = state | NodeState[current]
            except KeyError:
                logging.debug("Unable to find state '{}'".format(current))
                state = state | NodeState.UNKNOWN
        return state

    @classmethod
    def get_state_from_integer(cls, value: int):
        state = NodeState.NONE
        for current in cls:
            if value & current:
                state = state | current
        return state

    @classmethod
    def get_text(cls, state) -> str:
        text = []
        for current in cls:
            if current & state:
                text.append(current.name)
        return "+".join(text)


class Node(BaseModel):
    name: Annotated[str, StringConstraints(pattern=NODE_REGEX)]
    state: NodeState | None = None
    group: Annotated[str, StringConstraints(pattern=NODE_GROUP_REGEX)] | None = None
    boot_time: datetime | None = None
    reason: Annotated[str, StringConstraints(pattern=REASON_REGEX)] | None = None
    number_cores: int = 0
    number_gpus: int = 0
    memory_mb: int = 0
    gpu_memory_per_gpu_mb: int = 0


class NodeResponse(Node):
    name: str
    group: str | None = None
    reason: str | None = None
