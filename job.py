import logging
from datetime import datetime, timedelta
from enum import IntEnum, auto
from typing import Annotated, List, Union

from pydantic import BaseModel, StringConstraints

from .node import NODE_REGEX
from .user import USER_REGEX

# Allow any character and includes _ . and -
JOB_REGEX = r"^[a-zA-Z0-9-_.]+$"


class JobState(IntEnum):
    NONE = 0

    BOOT_FAIL = auto()
    CANCELLED = auto()
    COMPLETED = auto()
    CONFIGURING = auto()
    COMPLETING = auto()
    DEADLINE = auto()
    FAILED = auto()
    NODE_FAIL = auto()
    OUT_OF_MEMORY = auto()
    PENDING = auto()
    PREEMPTED = auto()
    RUNNING = auto()
    RESV_DEL_HOLD = auto()
    REQUEUE_FED = auto()
    REQUEUE_HOLD = auto()
    REQUEUED = auto()
    RESIZING = auto()
    REVOKED = auto()
    SIGNALING = auto()
    SPECIAL_EXIT = auto()
    STAGE_OUT = auto()
    STOPPED = auto()
    SUSPENDED = auto()
    TIMEOUT = auto()

    @classmethod
    def get_state_from_text(cls, text: str):
        # TODO accept short names
        for state in JobState:
            if state.name in text:
                return state
        logging.debug("Unable to find reason for {}".format(text))
        return JobState.NONE


class JobReason(IntEnum):
    NONE = 0
    AssocGrp = auto()
    AssociationJobLimit = auto()
    AssocMax = auto()
    AssociationResourceLimit = auto()
    AssociationTimeLimit = auto()
    BadConstraints = auto()
    BeginTime = auto()
    Cleaning = auto()
    DependencyNeverSatisfied = auto()
    Dependency = auto()
    FrontEndDown = auto()
    InactiveLimit = auto()
    InvalidAccount = auto()
    InvalidQOS = auto()
    JobHeldAdmin = auto()
    JobHeldUser = auto()
    JobLaunchFailure = auto()
    Licenses = auto()
    NodeDown = auto()
    NonZeroExitCode = auto()
    PartitionDown = auto()
    PartitionInactive = auto()
    PartitionNodeLimit = auto()
    PartitionTimeLimit = auto()
    Priority = auto()
    Prolog = auto()
    QOSGrp = auto()
    QOSJobLimit = auto()
    QOSMax = auto()
    QOSResourceLimit = auto()
    QOSTimeLimit = auto()
    QOSUsageThreshold = auto()
    ReqNodeNotAvail = auto()
    Reservation = auto()
    Resources = auto()
    SystemFailure = auto()
    TimeLimit = auto()
    WaitingForScheduling = auto()

    @classmethod
    def get_reason_from_text(cls, text: str):
        for reason in JobReason:
            if reason.name in text:
                return reason
        logging.debug("Unable to find reason for {}".format(text))
        return JobReason.NONE


class JobV2(BaseModel):
    name: Annotated[str, StringConstraints(pattern=JOB_REGEX)]
    job_id: int
    job_tasks: str = ""
    state: JobState
    reason: JobReason
    reason_raw: str
    # Resources
    # -1 thread means unknown
    threads: int
    cpus: int
    nodes: int
    memory_mb: int
    # Time
    start: Union[datetime, None]
    time_left: Union[timedelta, None]
    time_limit: timedelta
    submit_time: datetime


class JobResponseV2(JobV2):
    name: str


class JobReportV2(JobResponseV2):
    user: Annotated[str, StringConstraints(pattern=USER_REGEX)]
    # CPU
    cpu_util: float
    ram_util: float
    elapsed_time_s: int

    # Hardware
    nodes_list: List[Annotated[str, StringConstraints(pattern=NODE_REGEX)]]
    cpu_fraction_node: float
    ram_fraction_node: float
    gpus_fraction_node: float

    # GPU
    gpus: int
    gpu_memory_mb: int
    gpu_ram_util: float
    gpu_util: float

    # Energy
    consumed_energy_raw: float


class JobLongReportV2(JobReportV2):
    account: str
    constraints: str
    exit_code: int
    signal: int
    qos: str
    submit_line: str
    work_dir: str
    partition: str
