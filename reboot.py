from enum import Flag, auto
from typing import Annotated, List

from pydantic import BaseModel, StringConstraints

from .node import NODE_REGEX
from .reason import REASON_REGEX


class RebootType(Flag):
    NONE = 0
    NORMAL = auto()
    ASAP = auto()


class RebootNextState(Flag):
    NONE = 0
    RESUME = auto()
    DOWN = auto()


class Reboot(BaseModel):
    nodes: List[Annotated[str, StringConstraints(pattern=NODE_REGEX)]]
    reboot_type: RebootType
    next_state: RebootNextState | None = None
    reason: Annotated[str, StringConstraints(pattern=REASON_REGEX)] | None = None
