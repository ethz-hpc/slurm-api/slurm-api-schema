from typing import Annotated, Dict

from pydantic import BaseModel, StringConstraints

from .job import JobReason
from .user import USER_REGEX


class QueueStatistics(BaseModel):
    users: Dict[Annotated[str, StringConstraints(pattern=USER_REGEX)], int]
    reasons: Dict[JobReason, int]
    # If too many jobs, slurm use PRIORITY for
    # dependencies that have not been yet processed
    suspicious_priority: int
