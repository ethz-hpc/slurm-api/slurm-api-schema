from typing import Annotated

from pydantic import BaseModel, StringConstraints

# We have some weird usernames so need to allow _, dot, @ and -
USER_REGEX = r"^[a-zA-Z0-9_.@-]+$"


class User(BaseModel):
    username: Annotated[str, StringConstraints(pattern=USER_REGEX)]
