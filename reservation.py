from datetime import datetime
from enum import Flag, auto
from typing import Annotated, List, Union

from pydantic import BaseModel, StringConstraints

from .node import NODE_REGEX
from .user import USER_REGEX

# Allow only a to z, 0 to 9, dashes and underscores
RESERVATION_REGEX = r"^[a-zA-Z0-9-_]*$"


class ReservationFlag(Flag):
    NONE = 0
    MAINT = auto()
    IGNORE_JOBS = auto()
    TIME_FLOAT = auto()
    DAILY = auto()
    HOURLY = auto()
    WEEKLY = auto()
    WEEKDAY = auto()
    WEEKEND = auto()
    REPLACE = auto()
    REPLACE_DOWN = auto()
    FLEX = auto()
    MAGNETIC = auto()
    FIRST_CORES = auto()
    NO_HOLD_JOBS_AFTER = auto()
    OVERLAP = auto()
    PART_NODES = auto()
    PURGE_COMP = auto()  # Parameter not implemented
    SPEC_NODES = auto()
    ALL_NODES = auto()
    NO_HOLD_JOBS_AFTER_END = auto()

    # Not implemented
    # ANY_NODES = auto()
    # LICENSE_ONLY = auto()

    @classmethod
    def get_text(cls, flags):
        text = []
        for ref in ReservationFlag:
            if flags & ref:
                text.append(ref.name)

        return ",".join(text)

    @classmethod
    def get_flags(cls, text):
        flags = ReservationFlag.NONE
        if len(text) != 0:
            for current in text.split(","):
                # purge_comp requires an argument
                # for now we don't care
                if "PURGE_COMP" in current:
                    flags = flags | ReservationFlag.PURGE_COMP
                else:
                    flags = flags | ReservationFlag[current]
        return flags


class Reservation(BaseModel):
    name: Annotated[str, StringConstraints(pattern=RESERVATION_REGEX)]
    nodes: List[Annotated[str, StringConstraints(pattern=NODE_REGEX)]]
    # None means now
    start: Union[datetime, None] = None
    # for duration, -1 means infinity
    duration_min: int
    users: List[Annotated[str, StringConstraints(pattern=USER_REGEX)]]
    flags: ReservationFlag


class ReservationResponse(Reservation):
    name: str
    nodes: List[str]
    users: List[str]
