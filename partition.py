from datetime import timedelta
from enum import IntFlag, auto
from typing import Annotated, List, Union

from pydantic import BaseModel, StringConstraints

# Allow only a to z, 0 to 9, dashes and dot
PARTITION_REGEX = r"^[a-zA-Z0-9-._]+$"


class PartitionState(IntFlag):
    UP = auto()
    DOWN = auto()
    DRAIN = auto()
    INACTIVE = auto()

    @classmethod
    def get_state_from_text(cls, text: str):
        text = text.upper()
        try:
            return PartitionState[text]
        except KeyError:
            raise Exception("Unable to find state '{}'".format(text))


class Partition(BaseModel):
    name: Annotated[str, StringConstraints(pattern=PARTITION_REGEX)]
    state: PartitionState
    nodes: List[str]
    total_cpus: int
    total_nodes: int
    total_gpus: int
    max_time: Union[timedelta, None]
    default_partition: bool
    hidden: bool
